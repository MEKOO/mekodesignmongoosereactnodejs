import axios from 'axios';
import { SEARCH_NAME } from './constants';

export function searchNumePdv(search) {
  return {
    type: SEARCH_NAME,
    search,
    isFetching: false,
    requestedOnce: true,
  };
}

const getOption = (searchText) => ({
  method: "post",
  url: `http://localhost:4000/todos/search/`+ searchText,
});

export const fetchPostNeed = (searchText) => dispatch => {
  dispatch({
    type: SEARCH_NAME,
    isFetching: true,
    search: [],
  });
  axios(getOption(searchText))
    .then((res) => {
      dispatch(searchNumePdv(res.data));
    })
    .catch((_error) =>
      dispatch({
        type: SEARCH_NAME,
        status: "error",
        isFetching: false,
        requestedOnce: true,
        isSearch: [],
        _error: [],
      })
    );
};
