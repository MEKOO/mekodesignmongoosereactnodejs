// import produce from 'immer';
import { SEARCH_NAME } from './constants';

// The initial state of the App
export const initialState = {
  search: {
    // Pourquoi imbriquer un objet search dans search ?
    search: [],
  },
  isFetching: false,
  requestedOnce: false,
  error: '',
  status: '',
};

/* eslint-disable default-case, no-param-reassign */
const searchBarText = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_NAME:
      return Object.assign({}, state, {
        status: action.status,
        search: action,
        isFetching: action.isFetching,
        requestedOnce: action.requestedOnce,
        error: action.error,
      });
    default:
      return state;
  }
};

export default searchBarText;
