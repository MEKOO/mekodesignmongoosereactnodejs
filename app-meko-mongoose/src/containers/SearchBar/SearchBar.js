import React, { Component } from 'react';
import Autosuggest from "react-autosuggest";
import { connect } from "react-redux";
import { compose, bindActionCreators } from "redux";
import './searchBar.css';
import RecuperationList from '../RecuperationList';
import { debounce } from "throttle-debounce";
import { fetchPostNeed } from "./actions";


export const getSuggestionValue = (value) => () => value;

class SearchBar extends Component {
  constructor() {
    super();
    this.state = {
      value: "",
      suggestions: [],
      showItem: false,
    };
  }

    debounceFunc = debounce(300, false, (num) => {
        this.props.fetchUserList(num)
    });

  getSuggestions = (value) => {
     const suggestions = this.props.context.search;
     const inputValue = value;
    return inputValue.length <= 2
      ? []
      : suggestions.filter( (items) => {
            return `${items.name} ${items.lastname}`
            }
        );
  };

  onChange = (event, { newValue }) => {
        this.setState(
          {
            value: newValue
          },
          () => this.debounceFunc(newValue)
        );
  };

  getSuggestionValue = (suggestion) => {
    return `${suggestion.name}${suggestion.lastname}`;
  };

  renderSuggestion = (suggestion) => {
    return (
      <div>
        <span>{suggestion.name}</span> <span>{suggestion.lastname}</span>
      </div>
    );
  };

  onSuggestionsFetchRequested = (value) => {
    this.setState({
      suggestions: this.getSuggestions(value),
    });
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  onSuggestionSelected = (event, { suggestion }) => {
      this.setState({
        value: `${suggestion.name} ${suggestion.lastname}`,
        showItem: true,
      });
  };

  render() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: "Charcher un nom",
      value,
      onChange: this.onChange,
    };

    return (
      <div>
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          getSuggestionValue={getSuggestionValue(this.state.value)}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          renderSuggestion={this.renderSuggestion}
          onSuggestionSelected={this.onSuggestionSelected}
          inputProps={inputProps}
        />
        {this.state.showItem && (
          <RecuperationList
            suggestionItem={this.props.context}
            value={this.state.value}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  context: state.searchBar.search,
});

const mapDispatchToProps = (dispatch) => ({
  fetchUserList: bindActionCreators(fetchPostNeed, dispatch),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(SearchBar);