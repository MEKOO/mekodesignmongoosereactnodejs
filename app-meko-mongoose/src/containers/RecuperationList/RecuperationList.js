import React, { Component } from 'react';
import ItemlSelect from '../../components/ItemsSelect';

class RecuperationList extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  renderList = () => {
      const { suggestionItem } = this.props;
    if (suggestionItem !== undefined) {
              return (
                <ItemlSelect
                  children={this.props.suggestionItem.search.map((list) => {
                    return (
                      <div className="container">
                        <div className="row">
                          <div className="marginapp -10 col-md-6 bg-primary text-white rounded border border-primary">
                            <h2>
                              {list.name} {list.lastname}
                            </h2>
                            <p>{list.infos}</p>
                            <div className="todo-footer bg-success">
                              <strong>
                                <pan>ID : {list._id} </pan>
                              </strong>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                />
              );
    }else {
      return []
    }
  };

  render() {
    //console.log(this.props.suggestionItem, "litessitemm");
    return <div>{this.renderList()}</div>;
  }
}

export default RecuperationList;