import React from 'react';
import PropTypes from 'prop-types';

class ItemSelect extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        const { disabled, children } = this.props;
        return <div disabled={disabled}>{children}</div>;
    }
}

ItemSelect.propTypes = {
  disabled: PropTypes.bool,
  children: PropTypes.any,
};

export default ItemSelect;
