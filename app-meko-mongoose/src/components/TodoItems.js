import React from "react";
import PropTypes from "prop-types";
import ListTodo from './ListTodo';

class TodoItems extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { todos, onCheck } = this.props;
    return todos.map((todo) => {
      return <ListTodo key={todo._id} name={todo.name} onCheck={onCheck} />;
    });
  }
}

TodoItems.propTypes = {
  todos: PropTypes.arrayOf,
  onCheck: PropTypes.func,
};

export default TodoItems;
