import React from 'react';
import PropTypes from 'prop-types';

class Done extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
     Done = () => {
         const {list, check, delet} = this.props;
        return list.map(todo => {
            return (
            <ul id="done-items" className="list-unstyled">
                <li className="list-unstyled done">
                <label key={todo._id}  onClick={() => check(todo)}>
                    {todo.name}
                </label>
                <button
                    className="btn float-right paddingZero"
                    onClick={() => delet(todo._id)}
                >
                    <i aria-hidden="true" className="fa fa-trash red"></i>
                </button>
                </li>
            </ul>
            );
        }) 
    }

    render() {
        return (
            <div>
               {this.Done()} 
            </div>
        );
    }
}

Done.propTypes = {
  list: PropTypes.array,
  check: PropTypes.func,
  delet: PropTypes.func
};

export default Done;
