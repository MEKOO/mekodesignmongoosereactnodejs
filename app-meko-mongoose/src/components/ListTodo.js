import React from 'react';
import PropTypes from 'prop-types';

class ListTodo extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {name, onCheck } = this.props;
            return (
              <ul className="no-padding">
                <li className="list-unstyled">
                    <label onClick={() => onCheck(name)}>{name}</label>
                </li>
              </ul>
            );
    }
}

ListTodo.propTypes = {
    name: PropTypes.string,
    onCheck: PropTypes.func,
};

export default ListTodo;
