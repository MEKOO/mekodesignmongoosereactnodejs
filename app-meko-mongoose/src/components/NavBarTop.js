import React from 'react';
import PropTypes from 'prop-types';

class NavBarTop extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        const { title } = this.props;
       return (
            <nav className="navbar navbar-light bg-light">
                <span className="navbar-brand mb-0 h1">{title}</span>
            </nav>
            );
    }
}

NavBarTop.propTypes = {
    title: PropTypes.string
};

export default NavBarTop;
