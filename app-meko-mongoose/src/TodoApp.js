import React, { Component } from 'react';
import axios from 'axios';
import NavBarTop from './components/NavBarTop';
import SearchBar from './containers/SearchBar';


export default class TodoApp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            todos: [],
            done: [], 
            input: ''
        }
    }

    componentDidMount() {
        this.fetch()
    }

    fetch = () => {
        axios.get('http://localhost:4000/todos')
        .then(res => {
            this.filter(res.data)
        })
    }

    filter = (all) => {
        let todos = all.filter(todo => {
            return !todo.isCompleted
        })
        let done = all.filter(todo => {
            return todo.isCompleted
        })

        this.setState({
            todos: todos, 
            done : done
        })
    }

    add = (e) => {
        if (e.key === 'Enter') {
            let todo = {
              name: this.state.input,
              lastname: this.state.input,
              isCompleted: false,
            };
            axios.post('http://localhost:4000/todos/add', todo)
            .then(res => {
                console.log(res.data)
                this.fetch()
            })
            e.target.value = ''
        } 
    }
    check = (todo) => {
        axios.put('http://localhost:4000/todos/'+ todo._id, todo)
        .then(res => {
            console.log(res.data)
            this.fetch()
        })
    }

    delete = (id) => {
       axios.delete('http://localhost:4000/todos/'+ id)
       .then(res => {
           console.log(res.data)
           this.fetch()
       })
   }

    onChange = (input) => {
        this.setState({input: input})
    }
    
    render() {
        return (
          <div>
              <NavBarTop title="React Todo List Mekoo" />
              <div className="container">
                    <div className="row">
                      <div className="col-md-12">
                          <SearchBar />
                      </div>
                    </div>
              </div>
            </div>
        );
    }
}