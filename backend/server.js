const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
const Schema = mongoose.Schema;
const myRouter = express.Router();
//init bodyParser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//gestion cors 
app.use(cors())

//init port 
// Nous définissons ici les paramètres du serveur.
const hostname = 'localhost'; 
let port = 4000; 

// connect db mongoose //URL de notre base
const db = "mongodb+srv://mekoo:test123@cluster0.mnicj.mongodb.net/item-data";

// Nous connectons l'API à notre base de données
mongoose
  .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
      console.log("successfully connected to db")
  })
  .catch((err) => {
      console.log(err.message,'test error');
  });

//Schema pour modeliser nos objets pour la db blue print
const todoSchema = new Schema({
    name: String, 
    lastname: String, 
    email: String, 
    tel: String, 
    infos: String, 
    body: String, 
    isCompleted: Boolean
});

todoSchema.index({ name: "text", lastname: "text", tel: "text" });
const Todo = mongoose.model('todos', todoSchema);

app.use(myRouter);

//Routes
myRouter.route("/").all((req, res) => {
    res.json({message: "Bienvenue sur mon api Rest Mongoose", methode:req.methode});
});

//get id itel todo
myRouter.route("/todos/:todos_id").get(function (req, res) {
  Todo.findById(req.params.todos_id, function (err, todo) {
    if (err) res.send(err);
    res.json(todo);
  });
});

//get all documents from db
myRouter.route("/todos").get(function (req, res) {
      const todo = new Todo();
      // Nous récupérons les données reçues pour les ajouter à l'objet Piscine
      todo.name = req.body.name;
      todo.lastname = req.body.lastname;
      todo.email = req.body.email;
      todo.body = req.body.body;
      todo.infos = req.body.infos;
      todo.tel = req.body.tel;
      todo.isCompleted = req.body.isCompleted;
      Todo.find(function (err, items) {
        if (err) {
          res.status(400).send({ err: `${err} pas de recupération de data` });
        }
        res.status(200).send(items);
      });
}); 

//methode de filtre pour search en passant directement par 

/* myRouter.route("/todos/search/:query").get(function (req, res) {
  let query = req.params.query;
  
  Todo.find(
    { $text: { $search: query } },
    function (err, items) {
      console.log(query, "test params");
      if (err) {
        res.status(400).send({ err: `${err} pas de recupération de data` });
      }
      console.log(items, "query");
      res.status(200).send(items);
    }
  );
}); */

// find serach plain text mongoose en passant par mongoose sur la recherche
myRouter.route("/todos/search/:query").post(function (req, res) {
  let query = req.params.query;
  Todo.find(
    {
      name: { $regex: query, $options: "i" },
    },
    function (err, items) {
      console.log(query, "test params");
      if (err) {
        res.status(400).send({ err: `${err} pas de recupération de data` });
      }
      console.log(items, "query");
      res.status(200).send(items);
    }
  ).limit(5);
}); 

// to add list  post
myRouter.route("/todos/add").post((req, res) => {
  const todo = new Todo();
  // Nous récupérons les données reçues pour les ajouter à l'objet Piscine
    todo.name = req.body.name;
    todo.lastname = req.body.lastname;
    todo.email = req.body.email;
    todo.body = req.body.body;
    todo.infos = req.body.infos;
    todo.tel = req.body.tel;
    todo.isCompleted = req.body.isCompleted;
    //Nous stockons l'objet en base
    todo.save()
    .then(() => {
      res
        .status(200)
        .send({
          message: `
            ${todo.name} 
            ${todo.lastname} 
            ${todo.email} 
            ${todo.infos} 
            ${todo.body} 
            ${todo.isCompleted} 
            "todo Bravo, la piscine est maintenant stockée en base de données"`,
        });
    })
    .catch((err) => {
      res.status(400).json({ err : `${err} pas de data poster`});
      res.status(500).json("oh noes!");
      res.status(404).json("I dont have that");
    });
});

// update todo id
myRouter.route("/todos/:id").put((req, res) => {
  Todo.findByIdAndUpdate(req.params.id, req.body)
    .then((todo) => {
        todo.name = req.body.name;
        todo.lastname = req.body.lastname;
        todo.email = req.body.email;
        todo.tel = req.body.tel;
        todo.isCompleted = req.body.isCompleted;
        todo.save();
        res.status(200).send({
            message: `
            ${todo.name} 
            ${todo.lastname} 
            ${todo.email} 
            ${todo.infos} 
            ${todo.body} 
            ${todo.isCompleted}
            Bravo, mise à jour des données OK`,
        });
  })
  .catch((err) => {
         res.status(400).send({ err: `${err} pas de mise à jour de data` });
         res.status(500).json("oh noes!");
         res.status(404).json("I dont have that");
  });
});

// delete todo from db
myRouter.route("/todos/:id").delete(function (req, res) {
  Todo.findOneAndDelete(req.params.id, function () {
    res.status(200).send({ message: `todo is successfully deleted` });
  }).catch((err) =>
    res.status(400).send({ err: `error adding document ${err}` })
  );
});

//init port 
app.listen(port, hostname, () => {
  console.log("Mon serveur fonctionne sur http://" + hostname + ":" + port);
});
